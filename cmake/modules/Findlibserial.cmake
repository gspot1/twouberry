
if (LIBSERIAL_LIBRARIES AND LIBSERIAL_INCLUDE_DIRS)
    set(LIBSERIAL_FOUND TRUE)
else( )
    
    find_path(LIBSERIAL_INCLUDE_DIR
            NAMES libserial
            PATHS /usr/include
             /usr/local/include
             /opt/local/include
            )

    find_library(LIBSERIAL_LIBRARY
            NAMES serial
            PATHS /usr/lib/x86_64-linux-gnu

            )
    message("${LIBSERIAL_INCLUDE_DIR}")
    message("${LIBSERIAL_LIBRARY}")
    set(LIBSERIAL_INCLUDE_DIRS ${LIBSERIAL_INCLUDE_DIR})
    set(LIBSERIAL_LIBRARIES    ${LIBSERIAL_LIBRARY})

    if (LIBSERIAL_INCLUDE_DIRS AND LIBSERIAL_LIBRARIES)
        set(LIBSERIAL_FOUND TRUE)
    endif( )

    if (LIBSERIAL_FOUND)
        message(STATUS "Found Libserial:")
        message(STATUS " - Includes: ${LIBSERIAL_INCLUDE_DIRS}")
        message(STATUS " - Libraries: ${LIBSERIAL_LIBRARIES}")
    else( )
        message(FATAL_ERROR "Could not find Libserial")
    endif( )

    mark_as_advanced(LIBSERIAL_INCLUDE_DIRS LIBSERIAL_LIBRARIES)
endif( )