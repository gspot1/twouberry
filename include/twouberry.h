///  \file twouberry.h
///  \author Ivan Kraev (kraev.i@niuitmo.ru)
///  \brief 
///  \version 0.1
///  \date 2024-02-05
///  
///  @copyright Copyright (c) 2024


#include <stdint.h>

#pragma pack(push, 1)

/// \brief Структура с управлением для 4-х двигателей
typedef struct {
    uint8_t step_1;
    uint8_t step_2;
    uint8_t step_3;
    uint8_t step_4;
    uint8_t dir_1;
    uint8_t dir_2;
    uint8_t dir_3;
    uint8_t dir_4;
} twouberry_control;

#pragma pack(pop)