///  \file twouberry_udp.h
///  \author Ivan Kraev (kraev.i@niuitmo.ru)
///  \brief 
///  \version 0.1
///  \date 2024-02-05
///  
///  @copyright Copyright (c) 2024
///  

#include <bits/stdc++.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

/// \define TWOUBERRY_DEFAULT_UDP_PORT
/// \brief Дефолтный номер порта, который будет слуша UDP-сервер
#define TWOUBERRY_DEFAULT_UDP_PORT 6060

//#define MAXLINE 1024
   
typedef struct {
    int sockfd;
    struct sockaddr_in servaddr;
    struct sockaddr_in cliaddr;
    socklen_t len;
} twouberry_udp_handler;

/// \brief Инициализация сокета для работы с UDP
/// \param[out] handler - Управляющая структура для работы с UDP
/// \return 0 - Ок, Иначе - не ок
int twouberry_udp_init(twouberry_udp_handler* handler, uint16_t server_port);

/// \brief Получение данных по UDP
/// \param handler[in] - Управляющая структура для работы с UDP
/// \param data[out] - Буфер для полученных  данных
/// \param size[in] - Максимальный ожидаемый размер данных
/// \return Отрицательное число - ошибка, иначе - фактический размер полученных данных.
int twouberry_udp_recv(twouberry_udp_handler* handler, uint8_t* data, size_t size);

/// \brief Отправка данных по UDP
/// \param handler[in] - Управляющая структура для работы с UDP
/// \param data[in] - Буфер с данными для отправки
/// \param size[in] - Размер отправляемых данных
/// \return
int twouberry_udp_send(twouberry_udp_handler* handler, uint8_t* data, size_t size);


