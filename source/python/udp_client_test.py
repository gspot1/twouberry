import socket
import sys
import numpy as np
 

msgFromClient       = "Hello UDP Server"

bytesToSend         = str.encode(msgFromClient)

serverAddressPort   = ("127.0.0.1", 6060)

bufferSize          = sys.getsizeof(np.double)* 8

 

# Create a UDP socket at client side

UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

 
bytesToSend = np.array([23.,1.,230.,0.,234., 0., 55., 0.], dtype=np.double)
# Send to server using created UDP socket
print(sys.getsizeof(np.double))
print(sys.getsizeof(bytesToSend))
UDPClientSocket.sendto(bytesToSend, serverAddressPort)

 

msgFromServer = UDPClientSocket.recvfrom(bufferSize)

 

msg = "Message from Server {}".format(msgFromServer[0])

print(msg)