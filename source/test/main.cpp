/// \file main.cpp
/// \author  Краев Иван Юрьевич 🦍🦍🦍
/// \date 08.2022


#include <CppUTest/CommandLineTestRunner.h>

int main(int ac, char** av) {

    MemoryLeakWarningPlugin::turnOffNewDeleteOverloads( );
    return CommandLineTestRunner::RunAllTests(ac, av);
}