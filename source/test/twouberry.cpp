/// \file twouberry.cpp
/// \author Краев Иван Юрьевич 🦍🦍🦍
/// \date 01.2024
/// \brief Набор тестов для сервиса twouberry


#include <CppUTest/TestHarness.h>

// Client side implementation of UDP client-server model 
// #include <bits/stdc++.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <twouberry.h>
   
#define PORT     6060 
#define MAXLINE 1024 


TEST_GROUP(twouberry_udp_group) {
    int sockfd; 
    char buffer[MAXLINE]; 

    twouberry_control step_dirs_sent = {
        255,
        1,
        3,
        0,
        24,
        1,
        200,
        0
    };

    struct sockaddr_in     servaddr; 
   
    
       
    int n;
    socklen_t len; 
       
    void setup( ) override {
            // Creating socket file descriptor 
        if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
            perror("socket creation failed"); 
            exit(EXIT_FAILURE); 
        } 
    
        memset(&servaddr, 0, sizeof(servaddr)); 
        
        // Filling server information 
        servaddr.sin_family = AF_INET; 
        servaddr.sin_port = htons(PORT); 
        servaddr.sin_addr.s_addr = INADDR_ANY; 
    }
};

// Тестируем правильность определения message_id
TEST(twouberry_udp_group, twouberry_udp_server_test) {
    // std::system("./twouberry_udp_server");

    twouberry_control step_dirs_recvd = {0};
    sendto(sockfd, (const char *)&step_dirs_sent, sizeof(step_dirs_sent), 
        MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
            sizeof(servaddr)); 


    n = recvfrom(sockfd, (char *)&step_dirs_recvd, sizeof(step_dirs_sent),  
                MSG_WAITALL, (struct sockaddr *) &servaddr, 
                &len); 

    close(sockfd); 
    MEMCMP_EQUAL_TEXT(&step_dirs_sent, &step_dirs_recvd, sizeof(twouberry_control), "Wrong data recieved from server.");
}
