///  \file twouberry_service.cpp
///  \author Ivan Kraev (kraev.i@niuitmo.ru)
///  \brief Реализация сервиса для пересылки команд на управление двигателями полученных 
/// с UDP в UART
///  \version 0.1
///  \date 2024-02-05
///
///  @copyright Copyright (c) 2024
///  
///

#include <libserial/SerialStream.h>
#include <libserial/SerialPortConstants.h>

#include <twouberry_udp.h>
#include <twouberry.h>


const LibSerial::BaudRate baud_rates[18] = {
        LibSerial::BaudRate::BAUD_50,
        LibSerial::BaudRate::BAUD_75,
        LibSerial::BaudRate::BAUD_110,
        LibSerial::BaudRate::BAUD_134,
        LibSerial::BaudRate::BAUD_150,
        LibSerial::BaudRate::BAUD_200,
        LibSerial::BaudRate::BAUD_300,
        LibSerial::BaudRate::BAUD_600,
        LibSerial::BaudRate::BAUD_1200,
        LibSerial::BaudRate::BAUD_1800,
        LibSerial::BaudRate::BAUD_2400,
        LibSerial::BaudRate::BAUD_4800,
        LibSerial::BaudRate::BAUD_9600,
        LibSerial::BaudRate::BAUD_19200,
        LibSerial::BaudRate::BAUD_38400,
        LibSerial::BaudRate::BAUD_57600,
        LibSerial::BaudRate::BAUD_115200,
        LibSerial::BaudRate::BAUD_230400
};

/// @brief Функция для печати Step / Dir структуры
/// @param control_data[in] - Структура с данными управления 
void twouberry_print_control(twouberry_control control_data) {
    printf("Step 1 : %d\n"
           "Step 2 : %d\n"
           "Step 3 : %d\n"
           "Step 4 : %d\n"
           "Dir 1 : %d\n"
           "Dir 2 : %d\n"
           "Dir 3 : %d\n"
           "Dir 4 : %d\n",
            control_data.step_1,
            control_data.step_2,
            control_data.step_3,
            control_data.step_4,
            control_data.dir_1,
            control_data.dir_2,
            control_data.dir_3,
            control_data.dir_4); 
            
}

typedef struct {
    LibSerial::BaudRate uart_baudrate;
    uint16_t udp_port;
    char serial_device[128];
} twouberry_params;

/// \brief Получить аргументы из командной строки.
///
/// \param[out] params - параметры управляющего объекта сервиса.
/// \param[in]  argc - число аргументов командной строки.
/// \param[in]  argv - аргументы командной строки.
///
/// \return 0 - успеx, -1 - неудача.
int twouberry_get_params(twouberry_params* params, int argc, char* argv[ ]) {
    int opt = getopt(argc, argv, "p:b:d:");
    if (opt == -1) {
        return 0;
    }
    do {
        switch (opt) {
            case 'p': {
                params->udp_port = atoi(optarg);
                break;
            }
            case 'b': {
                int baud_rate_num = atoi(optarg);
                if ((baud_rate_num < 0) || (baud_rate_num >17)) {
                    printf("Invallid argument value!\n");
                    return -1;
                } else {
                    params->uart_baudrate = baud_rates[baud_rate_num];
                }
                break;
            }
            case 'd': {
                memset(params->serial_device, 0, 128);
                strcpy(params->serial_device, optarg);
                printf("Serial device selected: %s\n", params->serial_device);
                break;
            }
            default:
                return -1;
        }
    } while ((opt = getopt(argc, argv, "p:b:d:")) != -1);

    return 0;
}

int main(int argc, char* argv[]) {
    const char* dev_name = "/dev/ttyS0";
    /// Инициализируем дефолтные параметры
    twouberry_params params = {
            .uart_baudrate = LibSerial::BaudRate::BAUD_115200,
            .udp_port = TWOUBERRY_DEFAULT_UDP_PORT,
    };
    memcpy(params.serial_device, dev_name, 10);

    if (twouberry_get_params(&params, argc, argv) != 0) {
        printf("Usage: %s [-p] [port] [-b] [baudrate] [-d] [serial device]\nArgs:", argv[0]);
        printf("\n\tport: UDP port for server to listen to. Default: %d\n", params.udp_port);
        printf("\n\tbaudrate: Baud rate for serial transmission. Default: 115200 (16)\n");
        printf("\t\t0 - 50,"
               "\n\t\t1 - 75,"
               "\n\t\t2 - 110,"
               "\n\t\t3 - 134,"
               "\n\t\t4 - 150,"
               "\n\t\t5 - 200,"
               "\n\t\t6 - 300,"
               "\n\t\t7 - 600,"
               "\n\t\t8 - 1200,"
               "\n\t\t9 - 1800,"
               "\n\t\t10 - 2400,"
               "\n\t\t11 - 4800,"
               "\n\t\t12 - 9600,"
               "\n\t\t13 - 19200,"
               "\n\t\t14 - 38400,"
               "\n\t\t15 - 57600,"
               "\n\t\t16 - 115200,"
               "\n\t\t17 - 230400\n");
        printf("\n\tserial device: Device name for serial (UART) transmission. Default: \'%s\'\n", dev_name);
        return -1;
    }
    twouberry_control control_data;
    twouberry_udp_handler handler;
    if (twouberry_udp_init(&handler, params.udp_port)) {
        return -1;
    } else {
        std::cout<<"Server is up and listening to port "<< params.udp_port << std::endl;
    }

    std::string serial_string(params.serial_device);
    LibSerial::SerialStream uart_stream;
    try {
        uart_stream.Open(serial_string);
        uart_stream.SetBaudRate(params.uart_baudrate);
    } catch (LibSerial::OpenFailed) {
        std::cout<<"Error opening serial port! Device name passed: \'"<< serial_string << "\'"<< std::endl;
        return -1;
    }
    
    std::cout<<"Serial stream is associated with \' "<< serial_string << std::endl;
//    std::cout<<"Serial stream is associated with \' "<< serial_string <<" \' with baud rate set to " << params.uart_baudrate<< std::endl;


    while (true) {
        if (twouberry_udp_recv(&handler, (uint8_t*) &control_data, sizeof(twouberry_udp_handler)) < 0) {
            printf("UDP recieve error!\n");
            return -1;
        }
        twouberry_print_control(control_data);
        twouberry_udp_send(&handler, (uint8_t*)&control_data, sizeof(control_data));
        uart_stream.write((char*)&control_data, sizeof(control_data));
        std::cout<<"Recieved data sent to UART client."<<std::endl;
    }
    
       
    return 0; 
}