///  \file twouberry_udp.cpp
///  \author Ivan Kraev (kraev.i@niuitmo.ru)
///  \brief Файл с функциями для работы с UDP
///  \version 0.1
///  \date 2024-02-05
///  
///  \copyright Copyright (c) 2024
///  
///

// Server side implementation of UDP client-server model 
#include <twouberry.h>
#include <twouberry_udp.h>

#define TWOUBERRY_RPI_IP "192.168.88.252"

/// \brief Инициализация сокета для работы с UDP
/// \param[out] handler - Управляющая структура для работы с UDP
/// \return 0 - Ок, Иначе - не ок
int twouberry_udp_init(twouberry_udp_handler* handler, uint16_t server_port) {
    // Creating socket file descriptor 
    if ( (handler->sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
       
    memset(&handler->servaddr, 0, sizeof(struct sockaddr_in)); 
    // memset(&handler->cliaddr, 0, sizeof(struct sockaddr_in)); 
       
    // Filling server information 
    handler->servaddr.sin_family    = AF_INET; // IPv4 
    handler->servaddr.sin_addr.s_addr = inet_addr(TWOUBERRY_RPI_IP); 
    handler->servaddr.sin_port = htons(server_port);
       
    // Bind the socket with the server address 
    if (bind(handler->sockfd, (const struct sockaddr *)&handler->servaddr, sizeof(struct sockaddr_in)) < 0) { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 
    handler->len = sizeof(struct sockaddr_in);
    return 0;

}

/// \brief Получение данных по UDP
/// \param handler[in] - Управляющая структура для работы с UDP
/// \param data[out] - Буфер для полученных  данных
/// \param size[in] - Максимальный ожидаемый размер данных
/// \return Отрицательное число - ошибка, иначе - фактический размер полученных данных.
int twouberry_udp_recv(twouberry_udp_handler* handler, uint8_t* data, size_t size) {
    int n = recvfrom(handler->sockfd, data, size,  
                MSG_WAITALL, ( struct sockaddr *)&handler->cliaddr, 
                &handler->len);
    return n;        
}

/// \brief Отправка данных по UDP
/// \param handler[in] - Управляющая структура для работы с UDP
/// \param data[in] - Буфер с данными для отправки
/// \param size[in] - Размер отправляемых данных
/// \return 0 - Ок, иначе - Плохо
int twouberry_udp_send(twouberry_udp_handler* handler, uint8_t* data, size_t size) {
    sendto(handler->sockfd, data, size,  
           MSG_CONFIRM, (const struct sockaddr *) &handler->cliaddr, 
           handler->len); 
    return 0;       
}

