
// Client side implementation of UDP client-server model 
#include <bits/stdc++.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <twouberry.h>
   
#define TWOUBERRY_RPI_IP "192.168.88.252"   
#define PORT     6060 
#define MAXLINE 1024 
   
/// @brief Функция для печати Step / Dir структуры
/// @param control_data[in] - Структура с данными управления 
void twouberry_print_control(twouberry_control control_data) {
    printf("Step 1 : %d\n"
           "Step 2 : %d\n"
           "Step 3 : %d\n"
           "Step 4 : %d\n"
           "Dir 1 : %d\n"
           "Dir 2 : %d\n"
           "Dir 3 : %d\n"
           "Dir 4 : %d\n",
            control_data.step_1,
            control_data.step_2,
            control_data.step_3,
            control_data.step_4,
            control_data.dir_1,
            control_data.dir_2,
            control_data.dir_3,
            control_data.dir_4); 
            
}

// Driver code 
int main() { 
    int sockfd; 

    twouberry_control step_dirs = {
        255,
        234,
        222,
        134,
        1,
        0,
        1,
        1
    };

    struct sockaddr_in     servaddr; 
   
    // Creating socket file descriptor 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
   
    memset(&servaddr, 0, sizeof(servaddr)); 
       
    // Filling server information 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_port = htons(PORT); 
    servaddr.sin_addr.s_addr = inet_addr(TWOUBERRY_RPI_IP); ; 
       
    int n;
    socklen_t len; 
       
    sendto(sockfd, (const char *)&step_dirs, sizeof(step_dirs), 
        MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
            sizeof(servaddr)); 
    std::cout<<"Step / dir structure sent."<<std::endl; 


    n = recvfrom(sockfd, (uint8_t*)&step_dirs, sizeof(step_dirs),  
                MSG_WAITALL, (struct sockaddr *) &servaddr, 
                &len); 
    twouberry_print_control(step_dirs);
   
    close(sockfd); 
    return 0; 
}
